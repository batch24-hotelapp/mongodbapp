from fastapi import APIRouter
from kamar_api import *

router = APIRouter()

@router.get("/kamarbyid")
async def view_search_kamar_id(params:dict):
    result = search_kamar_id(**params)
    return result

@router.get("/kamarbyname")
async def view_search_kamar_by_name(params:dict):
    result = search_kamar_by_name(**params)
    return result

@router.get("/kamar")
async def view_search_kamar_by_name():
    result = search_kamar()
    return result

