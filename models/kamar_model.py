from pymongo import MongoClient
from bson.objectid import ObjectId

class database:
    def __init__(self):
        try:
            self.nosql_db = MongoClient()
            self.db = self.nosql_db.hotel
            self.mongo_col = self.db.kamar
            print("Database Connected")
        except Exception as e:
            print(e)

    def showKamar(self):
        result = self.mongo_col.find()
        return [item for item in result]

    def showKamarById(self, **params):
        result = self.mongo_col.find_one({"_id": ObjectId(params["id"])})
        return result

    def searchKamarByName(self, **params):
        query = {"nama" : {"$regex" : "{0}".format(params["nama"]), "$options" : "i"}}
        result = self.mongo_col.find(query)
        return result

    def insertKamar(self, **params):
        self.mongo_col.insert_one(params)

    def updateKamarById(self, **params):
        query_1 = {"_id":ObjectId(params["id"])}
        query_2 = {"$set": params["data"]}
        self.mongo_col.update_one(query_1, query_2)

    def deleteKamarById(self, id):
        query = {"_id": ObjectId(id["id"])}
        self.mongo_col.delete(query)

