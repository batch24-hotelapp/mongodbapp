from models.kamar_model import database as db

db = db()

def objIdToStr(obj):
    return str(obj["_id"])

def search_kamar_by_name(**params):
    data_list = []
    for kamar in db.searchKamarByName(**params):
        kamar["_id"] = objIdToStr(kamar)
        data_list.append(kamar)
    return data_list

def search_kamar():
    data_list = []
    for kamar in db.showKamar():
        kamar["_id"] = objIdToStr(kamar)
        data_list.append(kamar)
    return data_list

def search_kamar_id(**params):
    result = db.showKamarById(**params)
    result["_id"] = objIdToStr(result)
    return result

def ubahData(**params):
    try:
        db.updateKamarById(params)
    except Exception as e:
        print(e)
