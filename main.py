from fastapi import FastAPI
from kamar_router import router as kamar_router

app = FastAPI()

app.include_router(kamar_router)

app.get("/")
async def read_main():
    return {"message": "Hello Bigger Application!"}